import { useEffect, useState } from 'react';
import './App.scss';
import listarCatalogo from './service/catalogoService';



interface test {
  productName?: string
  items?: {
    0: {
      name?: string
      images?: {
        0: {
          imageUrl?: string
        }
      }
      sellers?: {
        0: {
          commertialOffer?: {
            Installments?: {
              0: {
                Value?: Number
              }
            }
          }
        }
      }
    }
  }
}

function App() {

  const [data, setData] = useState<test[]>()



  useEffect(() => {
    listarCatalogo().then(res => {
      setData(res.data)
    })
  }, [])




  const numberToReal = (data: number) => {
    const numero = data.toFixed(2).split('.');
    numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
    return numero.join(',');
  }

  return (

    // <div className="App">
    <div className="row">
      <div className="col-md-1 col-sm-1 col-xl-1 col-lg-1"></div>

      <div className="col-md-10 col-sm-10 col-xl-10 col-lg-10">
        <div className="row">
          {
            data ? data.map(el => {
              return (
                <>
                  <div className=" padding col-md-3 col-sm-3 col-xl-3 col-lg-3">
                    <div className="row overAllBox " id="daisy">

                      <div className="col-md-12 col-sm-12 col-xl-12 col-lg-12">
                        <img className="imagem" src={el?.items?.[0]?.images?.[0]?.imageUrl} alt="Minha Figura" />
                      </div>

                      <div className="row">
                        <div className="midthDots col-md-12 col-sm-12 col-xl-12 col-lg-12">
                          {el?.items?.[0].name}
                        </div>
                      </div>

                      <div className="center ">
                        <div className="midthDots valor col-md-12 col-sm-12 col-xl-12 col-lg-12">
                          {numberToReal(Number(el?.items?.[0].sellers?.[0]?.commertialOffer?.Installments?.[0]?.Value))}
                        </div>
                      </div>

                      <div className="center">
                        <div className="parcela col-md-12 col-sm-12 col-xl-12 col-lg-12">
                          10x de {numberToReal(Number(el?.items?.[0].sellers?.[0]?.commertialOffer?.Installments?.[0]?.Value) / 12)} sem Juros
                          </div>
                      </div>

                    </div>
                  </div>
                </>
              )
            }) : ''
          }
        </div>
      </div>
    </div>


  );


}



export default App;

